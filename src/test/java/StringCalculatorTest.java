import nl.han.oose.stringcalculator.StringCalculator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class StringCalculatorTest {

    @Test
    public void emptyStringReturnsZero()
    {
        StringCalculator stringcalculator = new StringCalculator();
        Assertions.assertEquals(0, stringcalculator.add(""));
    }

}
